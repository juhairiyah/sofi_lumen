<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tanaman extends Model
{
    protected $table='tanamen';

    protected $fillable=[
        'macam-macam_tanaman', 'harga_tanaman', 'stok_tanaman'
    ];
}
